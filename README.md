#  TP : My SD² Blog 

# Sujet

Ce tp est un challenge d'implémentation de la partie infra / pipeline automation d'un site web type "blog personnel" en JamStack.

# Mise en route

> Ce TP n'est **pas** un challenge de développement frontend. Nous vous fournissons un site Hugo préconfiguré, avec un thème basique qui permet d'afficher le contenu. Vous êtes néanmoins libre de toucher la config Hugo comme vous l'entendez.


Si vous voulez lancer le site dans un container docker local:
```
docker run -p 1313:1313 -v $(pwd):/site -w /site klakegg/hugo:0.107.0-ext-ubuntu-onbuild server --bind=0.0.0.0
```

[Runners Gitlab](https://docs.gitlab.com/runner/install/): shared-runners GitLab ($$), ou vos propres runners selon votre préférence.

___
___
___
## Implémentation niveau 1 (8/20) - Mise en place de Gitlab-CI

- [Gitflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow#:~:text=Qu'est%2Dce%20que%20Gitflow,Vincent%20Driessen%20de%20chez%20nvie.) à implémenter: une branche principale et des branches de features


    ![gitflow](./doc/gitflow.png)
    Schema 2 : Gitflow


- 2 stages à implémenter (see : [gitlab-ci](./.gitlab-ci.yml))

    1. build :
        * déclenché par un commit sur une branche
        * génère le site web et stocke les artefacts dans un registry d'artefacts
    2. deploy:
        * déclenché par un tag [semver](https://semver.org/lang/fr/) _(ex: `0.0.1`, `1.2.4-84ffa964f1a0de6533ed1be91da81cc87820e68c`, `2.7.4-beta`)_
        * pull les artefacts du registry et les déploie sur un environnement


### Registry d'artifacts

Le build du site web génère les artefacts: fichiers `.html`, `.css`, `.js`, etc. Ces fichiers doivent être stockés ensemble dans un registry, sous un identifiant unique qui permettra de reconnaitre la version du site web.

Le stage de deploy devra contenir un job capable de retrouver dans ce registry les artefacts à déployer sur un environnement donné.

> une stratégie viable (pas la seule): utiliser docker pour packager les artifacts et les stocker dans le [Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) de gitlab.

> Rappel: une image docker, c'est une tarball qui contient l'image de base + les modifications (layers) appliquées dans le Dockerfile. 

### Environnements

1 environnement = 1 service de stockage objet _(ex: AWS S3, Stockage Azure, GCP Cloud Storage, ...)_ servi par un [CDN](https://fr.wikipedia.org/wiki/R%C3%A9seau_de_diffusion_de_contenu).

Vous devez gérer **au minimum 2 environnements** : un environnement de production et un environnement de dev.

prod: environnement qui héberge le site web, auquel les utilisateurs finaux accèdent

dev: environnement qui héberge une version du site web en cours de modification. Les contributeurs y accèdent pour valider leurs modifications avant de merge sur master. 

### A ce stade, on doit avoir:
* Implémentation du gitflow de base : pipelines Build / Deploy executés en fonction des actions des contributeurs.
* 2 environnements: production et dev


___
___
___

## Implémentation niveau 2 (12/20) - Une CI qui tient la route

* Gérer plusieurs environnements de développement en parallèle: **chaque feature branche doit pouvoir être déployée sur un environnement de dev différent**, pour que la collaboration sur le projet soit possible qu'il y a ait 2, 10 ou 100 contributeurs.
* Gérer le cycle de vie des infrastructures: les environnements de développement doivent être détruits après 24h.
* Sécurisation des environnements: on ne doit pas pouvoir déployer en prod une version build à partir d'une feature branch.

### A ce stade, on doit avoir:

* gitflow de base fonctionnel
* 1 environnement de prod, et X environnements de dev créés / détruits au besoin.

___
___
___
## Implémentation niveau 3 (15/20) - [CMS](https://en.wikipedia.org/wiki/Content_management_system)


Plutôt que de commit directement sur le repository git, les contributeurs pourront se connecter à une interface qui permet d'éditer le contenu. 

_⚠️ Selon le CMS que vous choisissez, il est possible qu'il faille réviser le gitflow mis en place précédemment. N'hésitez pas à prendre cette liberté tant que le rendu final respecte les spécifications suivantes:_

* le "build" du site web doit rester indépendant du "deploy"
* le "deploy" en production doit rester une action manuelle à la main de l'admin
* le système des feature branch doit être respecté, c'est à dire:
    * Alice effectue une modif via le CMS.
    * Bob effectue une modif via le CMS.
    * => 2 branches sont crées sur le repository: une qui contient les modifications de Alice, l'autre qui contient les modifications de Bob. En aucun cas on ne commit directement sur master.


Exemples de cms:

### [Netlify](https://www.netlify.com/)
![netlify](./doc/netlify.png)


### [Strapi](https://en.wikipedia.org/wiki/Content_management_system)   
![strapi](./doc/strapi.png)


___
___
___

# ✨ Bonus (X/20) ✨

* Dans le cas où vous utilisez un CMS qui tourne sur une VM, implémentez une stratégie **On-demand Provisionning**: la VM est allumée uniquement lorsqu'un contributeur veut faire une modification, et éteinte après que la modification ait été effectuée.

* **Toute prise d'initiative / feature non-demandée mais argumentée pendant la soutenance sera prise en compte dans la note finale.**

___
___
___

# Workflow utilisateur type (comment l'exercice sera validé):

* En tant que contributeur, j'effectue une modification du contenu du site:
    * si il y a un CMS, depuis l'interface du CMS
    * sinon, directement sur gitlab: modification d'un fichier markdown du site, commit, push sur une branche `feature/X` et ouverture d'une merge request vers master.
* je créé un tag sur le dernier commit de ma branche (ex: `1.0.1-staging`) pour déclencher un déploiement vers environnement de dev
* Je consulte mon site sur l'environnement de dev pour valider les modifications.
* Validation de la merge request. Merge sur master.
* Je créé un tag sur le dernier commit de master (ex: `1.0.1`) pour déclencher un déploiement vers environnement de production
* Je constate les modifications sur le site de production

___
___
___
# Objectif du TP

* Découvrir une nouvelle approche du développement Web & serverless.
* Vous confronter aux problématique de pipeline automation, de versioning, d'environnements.
* Vous donner un exemple concret d'application du SD², pour un site web moins couteux pour vous comme pour la planète.